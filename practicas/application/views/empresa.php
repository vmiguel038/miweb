
    <h1> <center> QUIENES SOMOS  </center></h1>
</header>
<div class="logo">
    <h1> <center>  <img src="https://www.bapmex.com.mx/wp-content/uploads/2021/10/Somos.jpg" style="height: 400px; width: 900px;">   </center></h1>
<br>
<br>

<div class="alert alert-success text-center">
<h1> <center> Misiòn </center></h1>
</div>
<center><h2>
    Comercializar vehículos y prestar servicios de posventa de calidad a nivel nacional,
    superando las expectativas de los clientes, alcanzando altos niveles de productividad y rentabilidad,
    con personal calificado y comprometido, contribuyendo al desarrollo del Ecuador.
</h2></center>

<div class="alert alert-success text-center">
<h1> <center> Visiòn  </center></h1>
</div>
<center><h2>
    Ser líder en la comercialización de vehículos y servicios posventa en el sur del país gracias a la calidad de sus productos y servicios,
    a su gestión transparente, al profesionalismo de sus recursos humanos y a la lealtad de sus clientes.
</h2></center>

<div class="alert alert-success text-center">
<h1> <center> Política de Gestión Integral  </center></h1>
</div>
<center><h2>Mirasol S.A. es una empresa dedicada a la venta al por mayor y menor de vehículos nuevos y usados,
    accesorios y al mantenimiento y reparación de vehículos. Que se compromete a:
</h2></center>

<div class="alert alert-success text-center">
<h1> <center> MEDIO AMBIENTE  </center></h1>
</div>
<center><h2> Contribuir al desarrollo sostenible del entorno, a través del respeto al medio ambiente,
    utilizando medios eficientes, previniendo la contaminación,
     eliminando los desperdicios y el manejo responsable de residuos, a través del cumplimiento de las normas ambientales.
</h2></center>

<div class="alert alert-success text-center">
<h1> <center> MEJORA CONTINUA  </center></h1>
</div>
<center><h2> Nuestra organización se basa en una gestión por procesos y mejora continua que contribuyen a los índices de eficiencia
    y desempeño del Concesionario bajo estándares de marca y sistema de gestión implementados.
    Con concepto de innovación permanente, calidad y orientación a los resultados.
</h2></center>
