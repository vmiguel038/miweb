<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index( )

	{
			$this->load->view('header');
		$this->load->view('welcome_message');
		$this->load->view('footer');
}

public function auto()

{
		$this->load->view('header');
	$this->load->view('auto');
	$this->load->view('footer');
}


public function oferta()

{
		$this->load->view('header');
	$this->load->view('oferta');
	$this->load->view('footer');
}
public function posventa()

{
		$this->load->view('header');
	$this->load->view('posventa');
	$this->load->view('footer');
}
public function repuesto()

{
		$this->load->view('header');
	$this->load->view('repuesto');
	$this->load->view('footer');
}
public function empresa()

{
		$this->load->view('header');
	$this->load->view('empresa');
	$this->load->view('footer');
}
public function contacto()

{
		$this->load->view('header');
	$this->load->view('contacto');
	$this->load->view('footer');
}
}
